# Generated by Django 4.2.4 on 2023-08-30 19:11

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="receipt",
            name="tax",
            field=models.DecimalField(
                decimal_places=3, max_digits=10, null=True
            ),
        ),
    ]
