from django.urls import path
from receipts.views import (
    receipt_list,
    make_receipt,
    category_list,
    account_list,
    make_expensecagetory,
    make_account,
)

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", make_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", make_expensecagetory, name="create_category"),
    path("accounts/create/", make_account, name="create_account"),
]
